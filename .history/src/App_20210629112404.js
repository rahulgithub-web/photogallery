import './App.css';
import GalleryReact from './components/TabMenu/GalleryReact';

function App() {
  return (
    <GalleryReact />
  );
}

export default App;
